using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    private GameObject _target;
    private Vector3 _direction;

    [SerializeField]
    private float _despawnDistance;
    [SerializeField]
    private float _speed;


    public void SetTarget(GameObject target)
    {
        _target = target;
        _direction = (_target.transform.position - transform.position).normalized;
    }

    public void FixedUpdate()
    {
        transform.Translate(_direction * _speed * Time.fixedDeltaTime);
        if ((_target.transform.position - transform.position).magnitude >= _despawnDistance) {
        // Despawn enemy if too far from player
        Debug.Log("Despawning enemy!");
        Destroy(gameObject);
        }
    }

    public void Behaviour()
    {
        // I was thinking of using components for behaviours later on
        // So right now this method is just placeholder
    }

}
