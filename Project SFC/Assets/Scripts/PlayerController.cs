﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    [SerializeField]
    private Camera cam;
    [SerializeField]
    private GameObject _projectilePrefab, _p1, _p2;

    private bool canFire = true;

    public float _speed;
    public float _tbs; // Time Between Shots
    public float _screenBuffer;

    void Awake () {

    }

    // FixedUpdate is called once per physics calculation
    void FixedUpdate () {
        // Get player input
        float xInput = Input.GetAxis("Horizontal");
        float yInput = Input.GetAxis("Vertical");
        float fireInput = Input.GetAxis("Fire1");
        float posX, posY;

        // tl;dr - Get the WORLD position of the edge of the SCREEN horizontally
        float xBound = cam.ScreenToWorldPoint(new Vector3(Screen.width, 0f)).x;
        // tl;dr - Get the WORLD position of the edge of the SCREEN vertically
        float yBound = cam.ScreenToWorldPoint(new Vector3(0f, Screen.height)).y;

        // Base ship movement
        transform.Translate(new Vector3(xInput, yInput) * Time.deltaTime * _speed);

        // Clamp the possible bounds of the ship within the screen, with a buffer to avoid clipping
        posX = Mathf.Clamp(transform.position.x, -xBound + _screenBuffer, xBound - _screenBuffer);
        posY = Mathf.Clamp(transform.position.y, -yBound + _screenBuffer, yBound - _screenBuffer);
        transform.position = new Vector3(posX, posY);

        // Check input on Fire1 axis, start Coroutine fire()
        if (fireInput > 0)
        {
            StartCoroutine(fire());
        }
    }

    /// <summary>
    /// Simple coroutine to introduce delay between shots.
    /// Projectiles are also instantiated here.
    /// </summary>
    IEnumerator fire()
    {
        if (canFire)
        {
            canFire = false;
            Instantiate(_projectilePrefab, _p1.transform.position, Quaternion.identity);
            Instantiate(_projectilePrefab, _p2.transform.position, Quaternion.identity);
            yield return new WaitForSeconds(_tbs);
            canFire = true;
            yield break;
        }
        else yield break;
    }

}
