﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {

    [SerializeField]
    private float _interval;
    [SerializeField]
    private GameObject _player;
    [SerializeField]
    private GameObject[] _spawners;
    [SerializeField]
    private GameObject EnemyType;

    private float counter = 0.0f;

    public void Update()
    {
        counter += Time.deltaTime;
        while (counter >= _interval) {
        counter -= _interval;
        int idx = Random.Range(0, _spawners.Length);
        SpawnEnemyFrom(idx);
        }
    }

    public void SpawnEnemyFrom(int idx)
    {
        GameObject newenemy = Instantiate(EnemyType, _spawners[idx].transform);
        newenemy.GetComponent<Enemy>().SetTarget(_player);
    }

}
