﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileBehavior : MonoBehaviour {

    public float _speed;

    // Use this for initialization
    void Awake () {
        
    }
    
    // Update is called once per frame
    void Update () {
        transform.Translate(new Vector3(0, 1) * Time.deltaTime * _speed);
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        Debug.Log("Collision");
        if (col.collider.CompareTag("Boundaries"))
        {
            Destroy(gameObject);
        }
    }
}
